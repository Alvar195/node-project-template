module.exports = {
	connection: {
		host: process.env.DB_IP,
		user: 'user',
		password: 'userpwd'
	},
	database: process.env.userdb,
	table: 'some_table'
};
