const gulp = require('gulp'),
	spawn = require('child_process').spawn,
	browserify = require('gulp-browserify');

// Define base folders
const publicPath = 'public/',
	src = 'src/',
	css = `${publicPath}css/`,
	js = `${publicPath}js/`,
	dest = 'build/',
	ejs = 'views/',
	config = 'config/';

// const html = '/html/';

let node;
// Include plugins
const concat = require('gulp-concat'),
	rename = require('gulp-rename');
// let uglify = require('gulp-uglify');

gulp.task('scripts', function() {
	return gulp.src(publicPath + 'index.js') // Currently only index.js needs to be built. Change in the future if needed.
		.pipe(concat('index.js'))
		.pipe(rename({suffix: '.min'}))
		.pipe(gulp.dest(dest + publicPath));
});

gulp.task('code', function() {
	return gulp.src(src + '/**/*.js')
		.pipe(gulp.dest(dest + src));
});

gulp.task('config', function() {
	return gulp.src(config + '/**/*.js')
		.pipe(gulp.dest(dest + config));
});

gulp.task('ejs', function() {
	gulp.src(`${ejs}**/*.ejs`)
		.pipe(gulp.dest(dest+ejs));
});

gulp.task('css', function() {
	return gulp.src(css + '*.css').pipe(gulp.dest(dest + css));
});

gulp.task('js', function() {
	return gulp.src(js + '*.js').pipe(gulp.dest(dest + js));
});

gulp.task('browserify', function () {
	gulp.src(publicPath + 'client.js')
		.pipe(browserify(/*{
			 insertGlobals : true,
			 debug : !gulp.env.production
		 }*/))
		.pipe(rename('bundle.js'))
		.pipe(gulp.dest(dest + js))
});

gulp.task('server', function() {
	if (node) node.kill();
	node = spawn('node', ['build/public/index.min.js'], {stdio: 'inherit'});
	node.on('close', function(code) {
		if (code === 8) {
			gulp.log('Error detected, waiting for changes...');
		}
	});
});

gulp.task('watch', function() {
	gulp.watch('src/**/*.js', ['code', 'server']);
	gulp.watch('config/**/*.js', ['config', 'server']);
	gulp.watch('public/index.js', ['scripts', 'server']);
	gulp.watch('public/client.js', ['browserify', 'server']);
	gulp.watch('views/**/*.ejs', ['ejs', 'server']);
	gulp.watch('public/css/*.css', ['css', 'server']);
	gulp.watch('public/js/*.js', ['js', 'server']);
});

gulp.task('default', [
	'code',
	'config',
	'ejs',
	'css',
	'js',
	'browserify',
	'scripts',
	'watch',
	'server'
]);

process.on('exit', function() {
	if (node) node.kill()
});
