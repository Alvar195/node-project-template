// setup
const express = require('express'),
	bodyParser = require('body-parser'),
	morgan = require('morgan'),
	cookieParser = require('cookie-parser'),
	session = require('express-session'),
	flash = require('express-flash'),
	app = express(),
	port = process.env.PORT;

app.use(morgan('dev')); // log every request to the console
app.use(bodyParser.urlencoded({
	extended: true
}));
app.use(function(req, res, next) {
	res.set('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
	next();
});

const sessionStore = new session.MemoryStore;

app.use(cookieParser('my-precious-secret'));
app.use(session({
	cookie: { maxAge: 60000 },
	store: sessionStore,
	saveUninitialized: true,
	resave: 'true',
	secret: 'secret'
}));
app.use(flash());

app.use(bodyParser.json());
app.use("/css", express.static(__dirname + '/css'));
app.use("/images", express.static(__dirname + '/images'));
app.use("/assets", express.static(__dirname + '/assets'));
app.use("/js", express.static(__dirname + '/js'));
app.set('view engine', 'ejs'); // set up ejs for templating

app.listen(port);
console.log('Bits at  >> ' + port);

console.log(process.env['DB_IP']);
/**
 * Get routes, and get this show on the road.
 */
require('../src/routes/routes')(app);
