const config = require('../../config'),
	indexController = require('../controllers/index-controller'),
	badController = require('../controllers/bad-controller');

/**
 * Routes
 * @param app
 */
module.exports = function(app) {
	app.get('/', indexController.index);

	app.get('*', badController.wildcard);
};