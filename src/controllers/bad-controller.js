class BadController {
	wildcard(req, res) {
		return res.send('Something is wrong.');
	}
}

module.exports = new BadController();