const mysql = require('mysql'),
	dbConfig = require('../../db-config'),
	connection = mysql.createConnection({
		host: dbConfig.connection.host,
		user: dbConfig.connection.user,
		password: dbConfig.connection.password,
		database: dbConfig.database
	}),
	Promise = require('bluebird');

class DbService {
	query(sql) {
		return new Promise((accept, reject) => {
			connection.query(sql, (err, results) => {
				if (err) {
					return reject(err);
				}
				return accept(results);
			});
		});
	}

	insert(sql, data) {
		return new Promise((accept, reject) => {
			connection.query(sql, data, function (error, results) {
				if (error) {
					return reject(error);
				} else {
					return accept(results.insertId);
				}
			});
		});
	}

	escape(value) {
		return mysql.escape(value);
	}
}

module.exports = new DbService();