const config = {
	env: process.env.ENV,
	dev_mode: process.env.ENV === 'dev'
};

module.exports = config;